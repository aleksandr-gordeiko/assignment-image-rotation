CFLAGS=--std=c18 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
CC=gcc

all: out/main

out/bmp.o: src/bmp/bmp.c src/bmp/bmp.h out/image.o out/fileio.o out/status.o
	$(CC) -c $(CFLAGS) $< -o $@

out/image.o: src/image/image.c src/image/image.h
	$(CC) -c $(CFLAGS) $< -o $@

out/transform.o: src/image/transform.c src/image/transform.h out/image.o
	$(CC) -c $(CFLAGS) $< -o $@

out/fileio.o: src/fileio.c src/fileio.h
	$(CC) -c $(CFLAGS) $< -o $@

out/status.o: src/status.c src/status.h
	$(CC) -c $(CFLAGS) $< -o $@

out/util.o: src/util.c src/util.h
	$(CC) -c $(CFLAGS) $< -o $@

out/main.o: src/main.c out/bmp.o out/transform.o
	$(CC) -c $(CFLAGS) $< -o $@

out/main: out/main.o out/fileio.o out/image.o out/bmp.o out/transform.o out/status.o out/util.o
	$(CC) -o $@ $^

run:
	./out/main sample.bmp

clean:
	rm -f out/main* out/*.o