//
// Created by Aleksandr on 1/22/2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_FILEIO_H
#define ASSIGNMENT_IMAGE_ROTATION_FILEIO_H

#include <stdio.h>
#include "status.h"

enum open_status open_file(const char* path, const char *mode, FILE** file);
enum close_status close_file(FILE** file);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILEIO_H
