//
// Created by Aleksandr on 1/27/2021.
//

#include "status.h"

static const char* const bmp_read_message[] = {
        [BMP_READ_OK] = "Read BMP successfully",
        [BMP_READ_FAILED] = "An error occured while reading BMP",
        [BMP_READ_INVALID_HEADER] = "The BMP file has invalid header"
};

static const char* const bmp_write_message[] = {
        [BMP_WRITE_OK] = "Written BMP successfully",
        [BMP_WRITE_FAILED] = "An error occured while writing BMP"
};

void read_status_handler(enum bmp_read_status status) {
    alert(bmp_read_message[status]);
}

void write_status_handler(enum bmp_write_status status) {
    alert(bmp_write_message[status]);
}
