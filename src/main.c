//
// Created by Aleksandr on 1/22/2021.
//

#include "image/transform.h"
#include "bmp/bmp.h"

int main(int argc, char** argv) {

    if (argc < 2) alert("Not enough arguments" );
    if (argc > 2) alert("Too many arguments" );
    if (argc != 2) {
        alert("Usage: ./out/main BMP_FILE_NAME");
        return 1;
    }

    /*struct image some_image = {0};
    create_image(&some_image, 16, 32);
    write_some_image(&some_image);
    write_bmp("sample.bmp", &some_image);
    destroy_image(&some_image);*/

    const char* in_path = argv[1];
    const char* out_path = "rotated.bmp";

    struct image image = {0};

    const enum bmp_read_status read = read_bmp(in_path, &image);
    read_status_handler(read);
    if (read != BMP_READ_OK) return 2;

    struct image image_rotated = rotate_90deg_clockwise(&image);

    const enum bmp_write_status written = write_bmp(out_path, &image_rotated);
    write_status_handler(written);
    if (written != BMP_WRITE_OK) {
        destroy_image(&image);
        destroy_image(&image_rotated);
        return 3;
    }

    destroy_image(&image);
    destroy_image(&image_rotated);

    return 0;
}
