//
// Created by Aleksandr on 1/22/2021.
//

#include <stdbool.h>
#include "bmp.h"

static const uint16_t TYPE = 0x4D42;
static const uint32_t BI_SIZE = 40;
static const uint16_t BITS_PER_PIXEL = 3 * 8;
static const uint16_t BI_PLANES = 1;


static uint32_t get_bmp_padding(uint32_t width) {
    return (4 - width % 4) % 4;
}

//BMP read

static bool read_bmp_header(FILE** file, struct bmp_header* header) {
    if (fseek(*file, 0, SEEK_SET)) return false;
    if (fread(header, sizeof(struct bmp_header), 1, *file) != 1) return false;
    return true;
}

static bool read_bmp_image(FILE** file, struct image* image, uint32_t width, uint32_t height) {
    const size_t offset = sizeof(struct bmp_header);
    if (fseek(*file, offset, SEEK_SET)) return false;
    const uint32_t padding = get_bmp_padding(width);

    create_image(image, width, height);

    for (uint32_t i = 0; i < height; i++) {
        if (fread(get_pixel(image, 0, i), sizeof(struct pixel), width, *file) != width) {
            destroy_image(image);
            return false;
        }
        if (fseek(*file, padding, SEEK_CUR)) {
            destroy_image(image);
            return false;
        }
    }

    return true;
}

static bool is_header_valid(const struct bmp_header* header) {
    if (header->bfType != TYPE) return false;
    if (header->bOffBits != sizeof(struct bmp_header)) return false;
    if (header->biSize != BI_SIZE) return false;
    if (header->biBitCount != BITS_PER_PIXEL) return false;
    if (header->biSizeImage != header->biWidth * header->biHeight * sizeof(struct pixel)) return false;
    if (header->bfileSize != sizeof(struct bmp_header) +
            (sizeof(struct pixel)*(header->biWidth)+get_bmp_padding(header->biWidth))*(header->biHeight))
            return false;

    return true;
}

enum bmp_read_status read_bmp(const char* path, struct image* image) {
    FILE* file = NULL;

    if (open_file(path, "rb", &file) != OPEN_OK) return BMP_READ_FAILED;

    struct bmp_header header = {0};
    if (!read_bmp_header(&file, &header)) return BMP_READ_FAILED;
    if (!is_header_valid(&header)) return BMP_READ_INVALID_HEADER;

    if (!read_bmp_image(&file, image, header.biWidth, header.biHeight)) return BMP_READ_FAILED;

    if (close_file(&file) != CLOSE_OK) return BMP_READ_FAILED;
    return BMP_READ_OK;
}

//BMP write

static bool create_bmp_header(struct bmp_header *header, struct image *image) {
    if (!image) return false;

    const uint32_t width = image->width;
    const uint32_t height = image->height;

    header->bfType = TYPE;
    header->bfileSize = sizeof(struct bmp_header) + (sizeof(struct pixel)*width+get_bmp_padding(width))*height;
    header->bfReserved = 0;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = BI_SIZE;
    header->biWidth = width;
    header->biHeight = height;
    header->biPlanes = BI_PLANES;
    header->biBitCount = BITS_PER_PIXEL;
    header->biCompression = 0;
    header->biSizeImage = width*height*sizeof(struct pixel);
    header->biXPelsPerMeter = 0;
    header->biYPelsPerMeter = 0;
    header->biClrUsed = 0;
    header->biClrImportant = 0;

    return true;
}

enum bmp_write_status write_bmp(const char* path, struct image* image) {
    FILE* file = NULL;

    if (open_file(path, "wb", &file) != OPEN_OK) return BMP_WRITE_FAILED;

    struct bmp_header header = {0};
    if (!create_bmp_header(&header, image)) return BMP_WRITE_FAILED;
    const uint32_t padding = get_bmp_padding(header.biWidth);

    if (fwrite(&header, sizeof(struct bmp_header), 1, file) != 1) return BMP_WRITE_FAILED;
    for (uint32_t i = 0; i < header.biHeight; i++) {
        if (fwrite(get_pixel(image, 0, i),
                   sizeof(struct pixel),
                   header.biWidth,
                   file) != header.biWidth) return BMP_WRITE_FAILED;
        fwrite(get_pixel(image, 0, 0), sizeof(uint8_t), padding, file); //writing junk bytes
    }

    if (close_file(&file) != CLOSE_OK) return BMP_WRITE_FAILED;
    return BMP_WRITE_OK;
}
