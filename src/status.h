//
// Created by Aleksandr on 1/27/2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_STATUS_H
#define ASSIGNMENT_IMAGE_ROTATION_STATUS_H

#include "util.h"

enum open_status {
    OPEN_OK,
    OPEN_FAILED
};

enum close_status {
    CLOSE_OK,
    CLOSE_FAILED
};

enum bmp_read_status {
    BMP_READ_OK,
    BMP_READ_FAILED,
    BMP_READ_INVALID_HEADER
};

enum bmp_write_status {
    BMP_WRITE_OK,
    BMP_WRITE_FAILED
};

void read_status_handler(enum bmp_read_status status);
void write_status_handler(enum bmp_write_status status);

#endif //ASSIGNMENT_IMAGE_ROTATION_STATUS_H
