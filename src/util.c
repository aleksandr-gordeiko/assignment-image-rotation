//
// Created by Aleksandr on 1/27/2021.
//

#include "util.h"

void alert(const char *msg) {
    fprintf(stderr, "%s\n", msg);
}
