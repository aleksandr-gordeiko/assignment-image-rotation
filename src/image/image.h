//
// Created by Aleksandr on 1/22/2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <inttypes.h>
#include <stdlib.h>

struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* pixels;
};

void create_image(struct image* image, uint64_t width, uint64_t height);
void destroy_image(struct image* image);
struct pixel* get_pixel(const struct image* image, uint64_t x, uint64_t y);
void set_pixel(const struct image* image, uint64_t x, uint64_t y, struct pixel value);
void write_some_image(struct image* image);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
