//
// Created by Aleksandr on 1/22/2021.
//

#include "transform.h"

struct image rotate_90deg_clockwise(const struct image* image) {
    const uint64_t new_width = image->height;
    const uint64_t new_height = image->width;

    struct image new_image = {0};
    create_image(&new_image, new_width, new_height);

    for (uint64_t i = 0; i < image->height; i++) {
        for (uint64_t j = 0; j < image->width; j++) {
            set_pixel(&new_image, i, new_height - j - 1, *get_pixel(image, j, i));
        }
    }

    return new_image;
}
