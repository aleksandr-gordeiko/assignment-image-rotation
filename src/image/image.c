//
// Created by Aleksandr on 1/22/2021.
//

#include "image.h"

void create_image(struct image* image, uint64_t width, uint64_t height) {
    image->width = width;
    image->height = height;
    image->pixels = malloc(sizeof(struct pixel) * height * width);
}

void destroy_image(struct image* image) {
    free(image->pixels);
}

struct pixel* get_pixel(const struct image* image, uint64_t x, uint64_t y) {
    return &image->pixels[y * image->width + x];
}

void set_pixel(const struct image* image, uint64_t x, uint64_t y, struct pixel value) {
    *get_pixel(image, x, y) = value;
}

//cool function, used it for testing, but can be used in future
void write_some_image(struct image* image) {
    for (uint64_t i = 0; i < image->height; i++) {
        for (uint64_t j = 0; j < image->width; j++) {
            uint8_t shade = 0xFF * (i * image->width + j) / (image->width * image->height);
            set_pixel(image, j, i, (struct pixel){(shade%10) * 10, 0, shade});
        }
    }
}
