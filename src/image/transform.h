//
// Created by Aleksandr on 1/22/2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H

#include "image.h"

struct image rotate_90deg_clockwise(const struct image* image);

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H
