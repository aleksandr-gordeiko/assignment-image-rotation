//
// Created by Aleksandr on 1/22/2021.
//

#include "fileio.h"

enum open_status open_file(const char* path, const char *mode, FILE** file) {
    *file = fopen(path, mode);

    if (*file) return OPEN_OK;
    return OPEN_FAILED;
}

enum close_status close_file(FILE** file) {
    if (!fclose(*file)) return CLOSE_OK;
    return CLOSE_FAILED;
}
